# Test task
Note board client-server application.

## User experience:
* User opens client by url and sees input prompt (modal popup) where he should input his name
* After name has been filled user sees a white board (with other users notes if any).
* Each user can click anywhere on the board and random color note will appear in point of click (size of note is fixed).
* User can move his own notes by dragging them.
* User can input text to a note.
* User sees other users notes and is able to monitor their states (username, placement and text) in real time (but can't move/edit them)
* User sees his own notes marked somehow (by border for example)
* User should be able to reload a page without losing current authority (should keep his name and notes ownership)

## Terms
**Note** - just color rectangle with text and author name  
**White board** - page with plain background, with notes on it

## Client requirements:
Typescript  
React framework  
Any additional libs are allowed

## Server requirements:
NodeJs  
Typescript  
Any framework is welcome  
Rest api for user registration (username input)  
Websocket for clients synchronization  
There is no need for external storage (db), in-memory storage would be enough  

# Notes

## Structure
This directory contains the server (Express + Socket.IO) in the root, and the client in subdirectory "client" (Create React App).

Server is running on port _3001_, client is running on CRA-default _3000_.

## How to run

### Installation
To install required components for both parts:
`npm run install:all`

### Development mode
To start both parts in development mode (with HMR courtesy of Webpack Dev Server and CRA):
`npm run start:all`

### Build for "production"
To build both parts:
`npm run build:all`
