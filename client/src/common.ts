
export type NoteId = string;

export interface NotePosition {
  top: number;
  left: number;
}

export interface Note {
  id: string;
  username: string;
  text: string;
  backgroundColor: string;
  position: NotePosition,
  lastModified: number;
}

export type Notes = { [id: NoteId]: Note };

export function isNotePosition(x: unknown): x is NotePosition {
  return typeof x === 'object'
    && x !== null
    && typeof (x as NotePosition).left === 'number'
    && typeof (x as NotePosition).top === 'number'
}

export function isNote(x: unknown): x is Note {
  if (typeof x !== 'object' || x === null) return false;

  return typeof (x as Note).id === 'string'
    && typeof (x as Note).username === 'string'
    && typeof (x as Note).text === 'string'
    && typeof (x as Note).backgroundColor === 'string'
    && typeof (x as Note).lastModified === 'number'
    && isNotePosition((x as Note).position);
}

export function isNotes(x: unknown): x is Notes {
  if (typeof x !== 'object' || x === null) return false;

  return Object.values(x).every(isNote);
}

export interface ServerToClientEvents {
  'notes': (notes: Notes) => void;
  'notes/add': (addedNote: Note) => void;
  'notes/update': (updatedNote: Note) => void;
  'error': (message: string) => void;
}

export interface ClientToServerEvents {
  'notes': () => void;
  'notes/add': (newNote: { position: NotePosition, text: string }) => void;
  'notes/update': (updateNote: Note) => void;
  'notes/remove': (removeNoteId: NoteId) => void;
}
