import './App.css';
import Notes from './Notes';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useFetch } from '../hooks/useFetch';
import LoginPopup from './LoginPopup';


interface UserInfo {
  name: string;
}

function isUserInfo(x: any): x is UserInfo {
  return typeof x === 'object'
    && typeof x['name'] === 'string';
}

function App() {
  const userInfo = useFetch('/api/userInfo', isUserInfo);

  const isLoggedIn = !userInfo.loading && !userInfo.error && userInfo.response !== undefined;

  return (
    <div className='App'>
      {
        !isLoggedIn ?
          <LoginPopup />
        :
        <DndProvider backend={HTML5Backend}>
          <Notes owner={userInfo.response?.name} />
        </DndProvider>
      }
    </div>
  );
}

export default App;
