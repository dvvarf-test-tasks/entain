import React from 'react';

import ModalPopup from './ModalPopup';

function LoginPopup() {
  return (
    <ModalPopup>
      <form action='/api/login' method='post'>
        <div>
          <label htmlFor='username'>Username:</label>
        </div>
        <div>
          <input type='text' name='username' id='username' />
        </div>
        <div>
          <input type='submit' value='Enter' />
        </div>
      </form>
    </ModalPopup>
  );
};
 
export default LoginPopup;