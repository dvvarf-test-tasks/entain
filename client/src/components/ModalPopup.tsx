import React, { PropsWithChildren } from "react";

export interface ModalPopupProps {

}

function ModalPopup({ children }: PropsWithChildren<ModalPopupProps>) {
  return (
    <div className="ModalPopup">
      {children}
    </div>
  );
};
 
export default ModalPopup;