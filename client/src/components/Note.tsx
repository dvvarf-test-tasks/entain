import React, { useCallback } from 'react';
import { useDrag } from 'react-dnd';

import './Note.css';

import NoteContent from './NoteContent';

export interface NoteProps {
  id: string;
  left: number;
  top: number;
  backgroundColor: string;
  username: string;
  text: string;
  owned: boolean;
  onChange: (id: string, changedText: string) => unknown;
};

function Note({
  id,
  left,
  top,
  backgroundColor,
  username,
  owned = false,
  onChange,
  text: content,
}: NoteProps) {
  const [{ isDragging }, dragTargetRef] = useDrag(
    () => ({
      type: 'note',
      canDrag: owned,
      item: { id, left, top },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
    }),
    [id, left, top],
  );

  const handleNoteTextChange = useCallback((changedText: string) => {
    onChange(id, changedText);
  }, [id, onChange]);

  if (isDragging) {
    return <div ref={dragTargetRef} />;
  }

  return (
    <div
      ref={dragTargetRef}
      onClick={(ev: React.MouseEvent) => ev.stopPropagation()}
      className='Note'
      style={{ left, top, backgroundColor }}
      data-owned={owned ? 'true' : 'false'}
    >
      <div className='text-center'>
        <strong>{username}</strong>
      </div>
      <NoteContent
        owned={owned}
        text={content}
        onChange={handleNoteTextChange}
      />
    </div>
  );
}

export default Note;