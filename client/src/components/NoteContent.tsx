import React, { useCallback, useEffect, useRef, useState } from 'react';

const noop = () => {};

export interface EditableProps {
  text: string;
  owned?: boolean;
  onChange?: (text: string) => unknown;
}

function Editable({
  text,
  owned = false,
  onChange = noop,
}: EditableProps) {
  const [isEditable, setIsEditable] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  const previousText = useRef(text);

  // make this element editable on double click
  const handleDoubleClick = useCallback((ev: React.MouseEvent) => {
    if (!owned || isEditable) return;

    ev.stopPropagation();
    ev.preventDefault();

    setIsEditable(true);
  }, [isEditable, owned]);

  // commit text when focus is lost
  const handleBlur = useCallback(() => {
    const currentText = ref.current?.textContent ?? '';
    if (currentText !== previousText.current) {
      onChange(currentText);
    }

    previousText.current = text;

    setIsEditable(false);
  }, [onChange, text]);

  // commit text when 'enter' is pressed,
  // restore previous text when 'escape' is pressed
  const handleKeyDown = useCallback((ev: React.KeyboardEvent<HTMLDivElement>) => {
    if (ev.key === 'Enter' || ev.keyCode === 13) {
      handleBlur();
      return;
    }

    if (ev.key === 'Escape' || ev.key === 'Esc' || ev.keyCode === 27) {
      if (ref.current) {
        ref.current.textContent = previousText.current;
      }

      setIsEditable(false);
      return;
    }
  }, [handleBlur]);

  // set text to div when prop changes
  useEffect(() => {
    if (!ref.current) return;
    if (ref.current.textContent === text) return;

    ref.current.textContent = text;
  }, [text]);

  // focus when set to editable
  useEffect(() => {
    if (!isEditable) return;
    if (!ref.current) return;

    ref.current.focus();
  }, [isEditable]);

  return (
    <div
      ref={ref}
      onDoubleClick={handleDoubleClick}
      onKeyDown={handleKeyDown}
      onBlur={handleBlur}
      contentEditable={isEditable ? 'true' : 'false'}
      dangerouslySetInnerHTML={{ __html: text }}
      className='NoteContent'
    />
  );
};

export default Editable;