import React, { useCallback } from 'react';
import { useDrop } from 'react-dnd';

import './Notes.css';

import { useNotes } from '../hooks/useNotes';
import NoteComponent from './Note';

function Notes({ owner }: { owner: string | undefined }) {
  const { notes, addNote, updateNote } = useNotes();

  const sortedNotes = Object.values(notes).sort((noteA, noteB) => noteA.lastModified - noteB.lastModified);

  const handleNoteMove = useCallback(
    (noteId: string, left: number, top: number) => {
      const note = notes[noteId];

      if (note === undefined) return;

      updateNote({
        ...note,
        position: { left, top },
      });
    },
    [notes, updateNote],
  );

  const handleNoteTextChange = useCallback(
    (noteId: string, changedText: string) => {
      const note = notes[noteId];

      if (note === undefined) return;

      updateNote({
        ...note,
        text: changedText,
      });
    },
    [notes, updateNote]
  );

  const [, dropTargetRef] = useDrop(
    () => ({
      accept: 'note',
      drop(note: { id: string, left: number, top: number }, monitor) {
        const delta = monitor.getDifferenceFromInitialOffset();

        if (delta === null) return undefined;

        const left = Math.round(note.left + delta.x);
        const top = Math.round(note.top + delta.y);
        handleNoteMove(note.id, left, top);
        return undefined;
      },
    }),
    [handleNoteMove],
  );

  const handleClick = (ev: React.MouseEvent<HTMLDivElement>) => {
    ev.preventDefault();

    const rect = ev.currentTarget.getBoundingClientRect();
    const left = ev.clientX - rect.left;
    const top = ev.clientY - rect.top;

    addNote({ left, top });
  };

  return (
    <div
      ref={dropTargetRef}
      onClick={handleClick}
      className='Notes'
    >
      {
        sortedNotes.map((note) => {
          const { position, ...noteProps } = note;

          return (
            <NoteComponent
              key={note.id}
              {...position}
              {...noteProps}
              owned={note.username === owner}
              onChange={handleNoteTextChange}
            />
          );
        })
      }
    </div>
  );
}

export default Notes;