import { useEffect, useState } from 'react';

export const useFetch = <ResponseType>(url: string, validator: (x: any) => x is ResponseType, options?: RequestInit) => {
  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState<ResponseType | undefined>(undefined);

  useEffect(() => {
    setError(null);
    setLoading(true);

    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          throw new Error('Server returned not-OK.')
        }

        return response.json();
      })
      .then((response) => {
        if (validator(response)) {
          setResponse(response);
        } else {
          throw new Error('Invalid response received.')
        }
      })
      .catch(() => {
        setError(new Error(`Error while fetching ${url}.`))
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url, options, validator]);

  return { error, loading, response };
};