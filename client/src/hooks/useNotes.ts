import { useEffect, useRef, useState } from 'react';
import io, { Socket } from 'socket.io-client';

import { Note, NotePosition, Notes, ClientToServerEvents, ServerToClientEvents, isNote, isNotes } from '../common';

export const useNotes = () => {
  const [notes, setNotes] = useState<Notes>({});

  const socketRef = useRef<Socket<ServerToClientEvents, ClientToServerEvents> | null>(null);

  useEffect(() => {
    // on mount connect to socket server,
    // subscribe on events,
    // emit "notes" to request notes from server

    socketRef.current = io();

    socketRef.current.on('notes', (newNotes) => {
      if (!isNotes(newNotes)) {
        return;
      }

      // update all notes at once
      setNotes(newNotes);
    });

    const replaceNote = (note: Note) => {
      if (!isNote(note)) {
        return;
      }

      // update a single note
      setNotes((previousNotes) => ({
        ...previousNotes,
        [note.id]: note,
      }));
    };

    socketRef.current.on('notes/add', replaceNote);
    socketRef.current.on('notes/update', replaceNote);

    socketRef.current.emit('notes');

    return () => {
      if (socketRef.current !== null) {
        socketRef.current.disconnect();
      }
    }
  }, []);

  const addNote = (position: NotePosition, text: string = 'Double click to edit!') => {
    if (socketRef.current === null) return;

    socketRef.current.emit('notes/add', {
      position,
      text,
    });
  };

  const removeNote = (id: string) => {
    if (socketRef.current === null) return;

    socketRef.current.emit('notes/remove', id);
  };

  const updateNote = (updateNote: Note) => {
    if (socketRef.current === null) return;

    socketRef.current.emit('notes/update', updateNote);
  };

  return { notes, addNote, removeNote, updateNote };
}