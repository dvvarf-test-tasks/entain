// doc: https://create-react-app.dev/docs/proxying-api-requests-in-development/#configuring-the-proxy-manually

const { createProxyMiddleware } = require('http-proxy-middleware');

/** @param {import('express').Express} app */
module.exports = function (app) {
  app.use(
    createProxyMiddleware(['/api/','/socket.io/'], {
      target: 'http://localhost:3001/',
      changeOrigin: true,
      ws: true,
      logLevel: 'debug',
    })
  );
};