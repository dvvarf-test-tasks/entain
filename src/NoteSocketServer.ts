import http from 'http';
import express from 'express';
import { Server } from 'socket.io';
import { Notes, ClientToServerEvents, ServerToClientEvents, isNote, isNotePosition } from '../client/src/common';
import { SessionInfo } from './http';

export class NoteSocketServer {
  io: Server<ClientToServerEvents, ServerToClientEvents, {}>;
  notes: Notes = {};

  constructor(server: http.Server, sessionMiddleware: express.RequestHandler) {
    this.io = new Server(server, {
      cors: {
        origin: "*",
        methods: ["GET", "POST"],
        credentials: true
      }
    });

    const wrap = (middleware: any) => (socket: any, next: any) => middleware(socket.request, {}, next);
    this.io.use(wrap(sessionMiddleware));

    this.io.on('connection', (socket) => {
      console.log('user connected');

      // FIXME: unsafe cast, however I was unable to extend IncomingMessage in such a way, that tsc is happy
      const session = (socket.request as any).session as SessionInfo;

      if (!session || session.username === undefined) {
        socket.disconnect();
        return;
      }

      socket.on('notes', () => {
        console.log('user requests notes');
    
        socket.emit('notes', this.notes);
      });

      socket.on('notes/add', ({ position, text }) => {
        if (!isNotePosition(position) || typeof text !== 'string') {
          socket.emit('error', 'Malformed data for note creation.');
          return;
        }

        const randomColorHEX = Math.floor(Math.random()*16777215).toString(16);

        const id = Math.random().toString();

        this.notes[id] = {
          position,
          text,
          id,
          username: session.username,
          backgroundColor: `#${randomColorHEX}`,
          lastModified: Date.now(),
        };

        this.io.emit('notes/add', this.notes[id]);
      });

      socket.on('notes/update', (updateNote) => {
        if (!isNote(updateNote)) {
          socket.emit('error', 'Malformed data for note update.');
          return;
        }

        this.notes[updateNote.id] = {
          ...updateNote,
          lastModified: Date.now(),
        };
    
        this.io.emit('notes/update', this.notes[updateNote.id]);
      });

      socket.on('notes/remove', (removeNoteId) => {
        if (typeof removeNoteId !== 'string') {
          socket.emit('error', 'Malformed data for note removal.');
          return;
        }
    
        console.log('user removes a note');

        delete this.notes[removeNoteId];

        this.io.emit('notes', this.notes);
      });

      socket.on('disconnect', () => {
        console.log('user disconnected');
      });
    });
  }
}
