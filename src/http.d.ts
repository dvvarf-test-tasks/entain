import 'http';

interface SessionInfo {
  username: string;
}

declare module 'http' {
  export interface IncomingMessage {
    session?: SessionInfo | null;
  }
}

