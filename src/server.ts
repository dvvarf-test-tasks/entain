import http from 'http';
import path from 'path';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieSession from 'cookie-session';

import { NoteSocketServer } from './NoteSocketServer';


const app = express();
const server = http.createServer(app);

app.use(cors({
  origin: '*',
  methods: ['GET', 'POST'],
  credentials: true,
}));

const cookieSessionMidleware = cookieSession({
  name: 'note-board-session',
  keys: [':pC:jdab+tbCp|SZB9"'],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
});
app.use(cookieSessionMidleware);

const urlEncodedParser = bodyParser.urlencoded({ extended: true });
app.post('/api/login', urlEncodedParser, (req, res, next) => {
  if (!req.body.username) {
    res.sendStatus(403);
    next();

    return;
  }

  // create session
  req.session = {
    username: req.body.username,
  };

  // redirect user to root to reload the note board
  res.redirect('/');
});

app.get('/api/logout', (req, res, next) => {
  req.session = null;

  res.redirect('/');
});

app.get('/api/userInfo', (req, res, next) => {
  const session = req.session;

  if (session) {
    res.json({
      name: session?.username,
    });
  } else {
    res.sendStatus(403);
  }

  return next();
});

const clientPath = path.join(__dirname, '/../client/build');
app.use('/', express.static(clientPath));

new NoteSocketServer(server, cookieSessionMidleware);

server.listen(3001, () => {
  console.log('listening on *:3001');
});